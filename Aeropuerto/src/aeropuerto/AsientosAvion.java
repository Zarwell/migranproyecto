/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aeropuerto;

import static java.lang.Thread.sleep;

/**
 *
 * @author damt218
 */
public class AsientosAvion {
    
    private int asientosLibres;

    public AsientosAvion() {
        asientosLibres = 5;
    }
    
    public AsientosAvion(int asientosLibres) {
        this.asientosLibres = asientosLibres;
    }

    public int getAsientosLibres() {
        return asientosLibres;
    }

    public void setAsientosLibres(int asientosLibres) {
        this.asientosLibres = asientosLibres;
    }
    
    public void reservaAsientos(int numAsientosReserva){
        asientosLibres -=numAsientosReserva;
    }
    
    public synchronized void gestionAsientosLibresAvion(String nombre, int numAsientosReserva){
        /*
        Este método será ejecutado por cada uno de los hilos cliente.
        En principio cada cliente va a reservar tres asientos en la
        prueba.
        ▪ Funcionamiento: si hay asientos libres se les asigna al cliente (es
        decir, se llama el método reservaAsientos()).
        • Antes de hacer la resta, se dormirá 1 segundo, que hará
        dormir al hilo para simular un sistema más real con
        volumen de datos.
        • Tras despertar el hilo, llama al método
        reservaAsientos().
        */
        System.out.println(nombre + " tiene el Bloqueo");
        try{
            System.out.println(nombre + " hara una reserva de "+ numAsientosReserva + " asientos");
            sleep(1000);
        }catch(InterruptedException e){}
        if (asientosLibres >= numAsientosReserva){
            reservaAsientos(numAsientosReserva);
            System.out.println(nombre + " Reserva finalizada con Exito. las plazas libres son " + asientosLibres);
        }else{
            System.out.println("No hay plazas suficientes para el cliente " + nombre + " Las plazas libres son " + asientosLibres);
        }
        System.out.println(nombre + " deja el Bloqueo");
    }
}
