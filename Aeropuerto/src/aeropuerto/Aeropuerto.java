/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package aeropuerto;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author damt218
 */
public class Aeropuerto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        AsientosAvion asientos = new AsientosAvion();
        ClienteAvionHilo cliente1=new ClienteAvionHilo("Cliente 1", asientos);
        ClienteAvionHilo cliente2=new ClienteAvionHilo("Cliente 2", asientos);
        ClienteAvionHilo cliente3=new ClienteAvionHilo("Cliente 3", asientos);
        cliente1.start();
        cliente2.start();
        try {
            cliente1.join();
            cliente2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Aeropuerto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
