/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package aeropuerto;

/**
 *
 * @author damt218
 */
public class ClienteAvionHilo extends Thread{
    
    private String nombre;
    AsientosAvion asientos;

    public ClienteAvionHilo(String nombre, AsientosAvion asientos) {
        this.nombre = nombre;
        this.asientos = asientos;
    }
    
    @Override
    public void run() {
        asientos.gestionAsientosLibresAvion(nombre, 3);
    }
    
}
